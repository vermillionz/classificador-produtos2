
# EML 2 – Atividade 2

Alunos: Christian Espinoza e Michel Vasconcelos

## Configuração de CI/CD no GitLab

Neste documento descrevemos o pipeline de CI e CD para a aplicação de classificador de produtos em que incluímos no pipeline de CI testes da aplicação, análise de cobertura e no pipeline de CD a publicação da imagem no Docker hub e em seguida em ambientes de staging e produção no Heroku.

### Pipeline de CI

No arquivo .gitlab-ci.yml realizamos a configuração do pipeline de CI para que a cada atualização do código sejam disparadas ações de integração contínua de forma automatizada.

Como primeiro passo definimos a imagem a ser utilizada como a python:3.9.6-slim.

Em seguida criamos os stages de testes incluindo stages de verificação, qualidade e release que ao final ficou da seguinte forma:

![Imagem1](Imagem1.png)

#### Stage de verificação
No stage de verificação realizamos a verificação da versão do python e instalação dos requirements.

![Imagem2](Imagem2.png)

#### Stage de qualidade
No stage de qualidade realizamos testes e a análise de cobertura.

Fizemos a configuração dos testes em run_tests com a instalação dos requirements e realização de testes unitários com o unittest.

![Imagem3](Imagem3.png)

Em seguida adicionamos a biblioteca coverage que possibilita a análise de cobertura dos testes coletando quais linhas de código estão sendo acionadas pelos testes e visualisá-los em um relatório. Também incluímos uma verificação que falhará se a cobertura for menor que 100%.

![Imagem4](Imagem4.png)

### Pipeline de CD
#### Publicação da imagem no docke hub

O primeiro passo da criação deste pipe line de entrega contínua será a automação do deployment da imagem do classificador no Docker hub. Nesta automação será necessário incluir os seguintes passos: habilitação da imagem Docker, habilitação do serviço para execução desta imagem, login, build,  tag e push conforme abaixo.

![Imagem5](Imagem5.png)

Outro ponto importante é adicionar variáveis de usuário, senha e nome de registro da imagem para que o Gitlab faça o login e o registro correto no Docker hub.

#### Publicação da imagem no Heroku

O próximo passo que vamos seguir é o deployment em um servidor em ambiente de staging, neste caso no Heroku. Para isso vamos configurar como passos principais o login nesta plataforma, o push e em seguida o release da aplicação.

![Imagem6](Imagem6.png)

Em seguida publicamos de forma similar em ambiente produtivo. A única diferença é a definição para deployment manual em produção o que confere mais segurança.

![Imagem7](Imagem7.png)




